module VMCFG
	# ID задачи
	ID = "240518"

	# определяем подсеть
	SUBNET = "192.168.185."

	# проверять обновления репозитория?
	VM_BOX_CHECK_UPDATE = false

	# Использовать назначенные скрипты? Если =false, то скрипты в VM_CONFIG игнорируются
	SCRIPTS_USE = true

	# создаем список нужных виртуалок
	VM_CONFIG = [
	  { # имя ВМ 1
	    :vm_name => "dev01",
	    # образ системы Ubuntu 
	    :box => "ubuntu/focal64",
	    # настройки ВМ
		:vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "2048" },
	        { "--description" => "Host dev01"}
	    ],

	    :script => [
	    	"./files/scripts/docker.sh",
			"./files/scripts/app_prepare.sh"
	    ]
	  }
	  
	]
end