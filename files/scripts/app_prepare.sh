git clone https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01.git ./app
cd ./app
sudo apt install -y python3-venv
sudo apt install -y libpq-dev gcc
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

flask db upgrade
python3 seed.py
python3 -m unittest discover

pip install -r requirements-server.txt
gunicorn app:app -b 0.0.0.0:8000