#!/bin/bash
DOCKER_USER=vagrant
DOCKER_VERSION=26.1.4
# Скачиваем и устанавливаем докер
wget -qO- https://get.docker.com/ | sed 's/lxc-docker/lxc-docker-ce-$DOCKER_VERSION/' | sh
# Добавляем пользователя в группу докер
sudo usermod -aG docker $DOCKER_USER